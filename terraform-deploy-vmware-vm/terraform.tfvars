# Provider
provider_vsphere_host     = "vmware-vcenter508.arba.gov.ar"
provider_vsphere_user     = "juan.garriz"
provider_vsphere_password = "******"

# Infrastructure
deploy_vsphere_datacenter = "ARBA508"
deploy_vsphere_cluster    = "ClusterNSX"
deploy_vsphere_datastore  = "DS-EMC-TEST03"
deploy_vsphere_folder     = "/Testing"
deploy_vsphere_network    = "vxw-dvs-14596-virtualwire-20-sid-5018-LDS-D-440-POC"

# Guest
guest_name_prefix     = "prubatgarriz"
guest_template        = "Linux-Ubuntu-20.04-Template-LTS"
guest_vcpu            = "2"
guest_memory          = "2048"
guest_ipv4_netmask    = "24"
guest_ipv4_gateway    = "10.16.44.254"
guest_dns_servers     = "10.16.6.1"
guest_dns_suffix      = "arba.gov.ar"
guest_domain          = "arba.gov.ar"
guest_ssh_user        = "packer"
guest_ssh_password    = "VMware1!"
guest_ssh_key_private = "~/.ssh/id_ed25519"
guest_ssh_key_public  = "~/.ssh/id_ed25519.pub"
#guest_firmware        = "efi"

# Master(s)
master_ips = {
  "0" = "10.16.44.81"
#  "1" = "10.67.11.12"
#  "2" = "10.67.11.13"
}

# Worker(s)
worker_ips = {
  "0" = "10.16.44.82"
#  "1" = "10.67.11.22"
#  "2" = "10.67.11.23"
#  "3" = "10.67.11.24"
}
