# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = var.proveedor["user"]
  password       = var.proveedor["pass"]
  vsphere_server = var.proveedor["server"]

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source                 = "Terraform-VMWare-Modules/vm/vsphere"
  version                = "3.5.0"
  vmtemp                 = "linux-debian-server-11"
  instances              = 1
  vmname                 = "nfslonghorn"
  cpu_number             = 2
  num_cores_per_socket   = 1
  ram_size               = 2048
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp                   = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-27-sid-5024-LDS-P-81-Kubernetes-Prod" = ["10.16.8.200"] # To use DHCP create Empty list ["",""]
  }
  data_disk = {
    disk1 = {
      size_gb                   = 500,
      thin_provisioned          = true,
      data_disk_scsi_controller = 0
    },
  }
  ipv4submask       = ["24"]
  vmgateway         = "10.16.8.254"
  dc                = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list   = ["10.16.6.1", "10.16.6.2"]
  domain            = "arba.gov.ar"

}
