resource "vsphere_virtual_machine_snapshot" "snapshot_name" {
  virtual_machine_uuid = vsphere_virtual_machine.learn.id
  snapshot_name        = "snap of ${vsphere_virtual_machine.vm[count.index].name}"
  description          = "Creado por Terraform, pre modificacion recursos"
  memory               = "true"
  quiesce              = "true"
  remove_children      = "false"
  consolidate          = "true"
}