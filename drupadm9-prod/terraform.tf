variable "proveedor" {
  description = "Parametros para validar en el vcenter"
  type        = map(string)
  default = {
    user   = "juan.garriz"
    pass   = "******"
    server = "vmware-vcenter508.arba.gov.ar"
  }
}
