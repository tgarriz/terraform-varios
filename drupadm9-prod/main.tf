# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = var.proveedor["user"]
  password       = var.proveedor["pass"]
  vsphere_server = var.proveedor["server"]

  # if you have a self-signed cert
  allow_unverified_ssl = true
}

  
module "server-linuxvm" {
  source                 = "Terraform-VMWare-Modules/vm/vsphere"
  version                = "3.5.0"
  vmtemp                 = "linux-ubuntu-server-22-04-lts"
  instances              = 1
  vmname                 = "drupadm9"
  cpu_number             = 4
  num_cores_per_socket   = 4
  ram_size               = 8196
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp                   = "ClusterNSX-T/Resources"
  network = {
    "SEG-P-10-WEB" = ["10.16.1.3"] # To use DHCP create Empty list ["",""]
  }
  #data_disk = {
  #  disk1 = {
  #    size_gb                   = 200,
  #    thin_provisioned          = true,
  #    data_disk_scsi_controller = 0
  #  },
  #}
  ipv4submask       = ["24"]
  vmgateway         = "10.16.1.254"
  dc                = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list   = ["10.16.6.1", "10.16.6.2"]
  domain            = "arba.gov.ar"

}
