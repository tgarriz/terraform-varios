# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "juan.garriz"
  password       = "Catalina1998"
  vsphere_server = "vcenter508.arba.gov.ar" 

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source        = "Terraform-VMWare-Modules/vm/vsphere"
  version       = "3.5.0"
  vmtemp        = "ubuntu-Focal-20.04LTS-Template"
  instances     = 3 
  vmname        = "rkem"
  cpu_number    = 8
  ram_size      = 16256
  num_cores_per_socket   = 8
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp          = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-26-sid-5023-LDS-T-210-Kubernetes" = ["10.16.21.1","10.16.21.2","10.16.21.3"] # To use DHCP create Empty list ["",""]
  }

  vmgateway = "10.16.21.254"
  dc        = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list  = ["10.16.6.1", "10.16.6.2"]
  domain = "test.arba.gov.ar"
  
}
