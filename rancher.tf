# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = ""
  password       = ""
  vsphere_server = "" 

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source        = "Terraform-VMWare-Modules/vm/vsphere"
  version       = "3.0.0"
  vmtemp        = "ubuntu-Focal-20.04LTS-Template"
  instances     = 3 
  vmname        = "rkeqa-m"
  cpu_number    = 4
  ram_size      = 8192
  cpu_hot_add_enabled    = true
  cpu_hot_remove_enabled = true
  memory_hot_add_enabled = true
  vmrp          = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-26-sid-5023-LDS-T-210-Kubernetes" = ["10.16.21.20","10.16.21.21","10.16.21.22"] # To use DHCP create Empty list ["",""]
  }  

  vmgateway = "10.16.21.254"
  dc        = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list  = ["10.16.6.1", "10.16.6.2"]
  domain = "clapps.test.arba.gov.ar"
  
}
