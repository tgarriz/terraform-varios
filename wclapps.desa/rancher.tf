# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "juan.garriz"
  password       = "******"
  vsphere_server = "vcenter508.arba.gov.ar" 

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source        = "Terraform-VMWare-Modules/vm/vsphere"
  version       = "3.0.0"
  vmtemp        = "ubutu-20.04LTS-400gbhd"
  instances     = 5 
  vmname        = "rkedesa-w"
  cpu_number    = 8
  ram_size      = 16256
  num_cores_per_socket   = 1
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp          = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-26-sid-5023-LDS-T-210-Kubernetes" = ["10.16.21.33","10.16.21.34","10.16.21.35","10.16.21.36","10.16.21.37"] # To use DHCP create Empty list ["",""]
  }  

  vmgateway = "10.16.21.254"
  dc        = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list  = ["10.16.6.1", "10.16.6.2"]
  domain = "clapps.test.arba.gov.ar"
  
}
