# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "juan.garriz"
  password       = "Catalina1998"
  vsphere_server = "vcenter508.arba.gov.ar" 

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source        = "Terraform-VMWare-Modules/vm/vsphere"
  version       = "3.4.1"
  vmtemp        = "linux-debian-server-11"
  instances     = 1
  vmname        = "nbk-haproxy44"
  cpu_number    = 2 
  ram_size      = 2048
  num_cores_per_socket   = 1
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp          = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-20-sid-5018-LDS-D-440-POC" = ["10.16.44.253"] # To use DHCP create Empty list ["",""]
  }
  ipv4submask  = ["24"]
  vmgateway = "10.16.44.254"
  dc        = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list  = ["10.16.6.1", "10.16.6.2"]
  domain = "test.arba.gov.ar"
  
}



