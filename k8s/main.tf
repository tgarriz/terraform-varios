# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = var.proveedor["user"]
  password       = var.proveedor["pass"]
  vsphere_server = var.proveedor["server"]

  # if you have a self-signed cert
  allow_unverified_ssl = true
}

# Deploy 2 linux VMs
module "server-linuxvm" {
  source        = "Terraform-VMWare-Modules/vm/vsphere"
  version       = "3.5.0"
  vmtemp        = "Debian-bullseye(11)-Template-64-PAM-LVM"
  instances     = 3 
  vmname        = "k8s-"
  cpu_number    = 4
  ram_size      = 8192
  num_cores_per_socket   = 8
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp          = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-20-sid-5018-LDS-D-440-POC" = ["10.16.44.73","10.16.44.74","10.16.44.75"] # To use DHCP create Empty list ["",""]
  }  
  ipv4submask       = ["24"]
  vmgateway = "10.16.44.254"
  dc        = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list  = ["10.16.6.1", "10.16.6.2"]
  domain = "test.arba.gov.ar"
  
}

