# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = var.proveedor["user"]
  password       = var.proveedor["pass"]
  vsphere_server = var.proveedor["server"]

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source                 = "Terraform-VMWare-Modules/vm/vsphere"
  version                = "3.5.0"
  vmtemp                 = "RedHat8.5-Template-64-LVM"
  instances              = 3
  vmname                 = "idm-"
  cpu_number             = 4
  num_cores_per_socket   = 4
  ram_size               = 16384
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp                   = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-10-sid-5008-LDS-T-170-WebTesting" = ["10.16.17.31","10.16.17.32","10.16.17.33"] # To use DHCP create Empty list ["",""]
  }
  ipv4submask       = ["24"]
  vmgateway         = "10.16.17.254"
  dc                = "ARBA508"
  datastore_cluster = "Datastores-IBM"
  dns_server_list   = ["10.16.6.1", "10.16.6.2"]
  domain            = "test.arba.gov.ar"

}
