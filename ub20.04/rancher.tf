# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = "juan.garriz"
  password       = "******"
  vsphere_server = "vmware-vcenter508.arba.gov.ar" 

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source        = "Terraform-VMWare-Modules/vm/vsphere"
  version       = "3.5.0"
  vmtemp        = " Linux-Ubuntu-20.04-Template-LTS"
  instances     = 1
  vmname        = "molecule-t"
  cpu_number    = 4
  ram_size      = 8196
  cpu_hot_add_enabled    = true
  cpu_hot_remove_enabled = true
  memory_hot_add_enabled = true
  vmrp          = "ClusterNSX/Resources"
  network = {
    "vxw-dvs-14596-virtualwire-20-sid-5018-LDS-D-440-POC" = ["10.16.44.50"] # To use DHCP create Empty list ["",""]
  }
#  data_disk = {
#    disk0 = {
#      size_gb                   = 70,
#      thin_provisioned          = true,
#      data_disk_scsi_controller = 0,
#    }
#  } 

  vmgateway = "10.16.44.254"
  dc        = "ARBA508"
  datastore_cluster = "Datastores-EMC.TEST"
  dns_server_list  = ["10.16.6.1", "10.16.6.2"]
  domain = "test.arba.gov.ar"

}

resource "vsphere_virtual_machine" "kubernetes_controller" {
    provisioner "remote-exec" {
    inline = [
      "apt update",
      "apt-y upgrade"
      ]
    connection {
      type          = "ssh"
      user          = "root"
      private_key   = "eflhr.14"
      host          = "10.16.44.50"
    }
  }

}

