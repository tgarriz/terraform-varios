# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = var.proveedor["user"]
  password       = var.proveedor["pass"]
  vsphere_server = var.proveedor["server"]

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source                 = "Terraform-VMWare-Modules/vm/vsphere"
  version                = "3.8.0"
  vmtemp                 = "Linux-Ubuntu-20.04-Template-LTS-Contenedores"
  instances              = 1
  vmname                 = "rkeqa-w"
  vmnameformat		 = "%02d"
  vmstartcount		 = "6"
  cpu_number             = 8
  num_cores_per_socket   = 4 
  ram_size               = 16256
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp                   = "ClusterNSX-T/Resources"
  network = {
    "SEG-T-210-Kubernetes" = ["10.16.21.39"] # To use DHCP create Empty list ["",""] 
  }
  data_disk = {
    disk1 = {
      size_gb                   = 1300,
      thin_provisioned          = true,
      data_disk_scsi_controller = 0
    },
  }
  ipv4submask       = ["24"]
  vmgateway         = "10.16.21.254"
  dc                = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list   = ["10.16.6.1", "10.16.6.2"]
  domain            = "test.arba.gov.ar"

}
