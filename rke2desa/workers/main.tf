# Configure the VMware vSphere Provider
provider "vsphere" {
  user           = var.proveedor["user"]
  password       = var.proveedor["pass"]
  vsphere_server = var.proveedor["server"]

  # if you have a self-signed cert
  allow_unverified_ssl = true
}


# Deploy 2 linux VMs
module "server-linuxvm" {
  source                 = "Terraform-VMWare-Modules/vm/vsphere"
  version                = "3.8.0"
  vmtemp                 = "Template-Ubuntu22.04-Containers"
  instances              = 5
  vmname                 = "rke2desa-w"
  vmfolder               = "Desarrollo/Kube"
  cpu_number             = 4
  num_cores_per_socket   = 4 
  ram_size               = 8196
  cpu_hot_add_enabled    = false
  cpu_hot_remove_enabled = false
  memory_hot_add_enabled = false
  vmrp                   = "ClusterNSX-T/Resources"
  network = {
    "SEG-T-210-Kubernetes" = ["10.16.21.63","10.16.21.64","10.16.21.65","10.16.21.66","10.16.21.67"] # To use DHCP create Empty list ["",""]
  }
  data_disk = {
    disk2 = {
      size_gb                   = 1300,
      thin_provisioned          = true,
      data_disk_scsi_controller = 0
    }
  }
  ipv4submask       = ["24","24","24","24","24"]
  network_type 	    = ["e1000","e1000","e1000","e1000","e1000",]
  vmgateway         = "10.16.21.254"
  dc                = "ARBA508"
  datastore_cluster = "Datastores-EMC"
  dns_server_list   = ["10.16.6.1", "10.16.6.2"]
  domain            = "arba.gov.ar"

}
